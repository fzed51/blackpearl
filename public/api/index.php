<?php
declare(strict_types=1);

chdir(__DIR__ . '/../..');
require './vendor/autoload.php';

(new \Api\App())->run();
