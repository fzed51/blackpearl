const genConf = require("@fzed51/webpack-config");
const path = require("path");

const config = genConf({
    useReact: true,
    htmlWebpackPlugin: true,
    useTypescript: false,
    cleanOutput: false
})

config.entry = {
    main: path.resolve(__dirname, "./clientapp/index.jsx")
}
config.output = {
    path: path.resolve(__dirname, './public')
}

module.exports = config;
