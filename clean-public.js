const fs = require('fs');

const excludefile = [
    'api',
    'favicon.ico'
];

fs.readdir('./public', {withFileTypes: true}, (err, files) => {
    files.forEach((file) => {
        if (!excludefile.includes(file.name)) {
            const fullName = './public/' + file.name;
            fs.unlink(fullName, (err) => {
                if (err) {
                    throw err;
                }
                console.log(fullName + ' est supprimé');
            });
        }
    })
});