<?php

declare(strict_types=1);

namespace CommonApi;

use RuntimeException;

/**
 * Class App
 * @package CommonApi
 */
abstract class App extends \Slim\App
{
    /**
     * App constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        Parent::__construct($config);
        $this->register(ModuleInterface::class); /* enregistrer ici les modules */
    }

    protected function getConfig(array $config = []): array
    {
        return $config;
    }

    /**
     * Méthode pour ajouter des éléments au container
     */
    protected function setDependencies(): void
    { }


    /**
     * Méthode pour ajouter des middleware à l'application
     */
    protected function setMiddleware(): void
    { }

}
