<?php

return [
    'db' => [
        'provider' => 'sqlite',
        'file' => './db/data.sqlite',
        'host' => '',
        'port' => '',
        'dbname' => '',
        'user' => '',
        'pass' => '',
    ]
];