create table sms (
    id integer primary key autoincrement
);
---
alter table sms add column time text;
---
alter table sms add column member_id text;
---
alter table sms add column recipient_number text;
---
alter table sms add column message text;
---
alter table sms add column sms_id text;
