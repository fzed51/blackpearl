create table account (
    id integer primary key autoincrement
);
---
alter table account add column time text;
---
alter table account add column member_id text;
---
alter table account add column nb_sms integer default 0;
---
alter table account add column comment text;
