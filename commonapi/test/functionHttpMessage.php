<?php

namespace Test;

use Slim\Http\Body;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Uri;

/**
 * @param string $uri
 * @param array $data
 * @param array $headers
 * @return Request
 */
function makePostRequest(
    string $uri,
    array $data,
    array $headers = []
) {
    $method = "POST";
    $uri = Uri::createFromString($uri);
    $headers = new Headers($headers);
    $serverParams = [];
    $cookies = [];
    $request = new Request($method, $uri, $headers, $cookies, $serverParams, new Body(makeBody()));
    return $request->withParsedBody($data);
}

/**
 * @param string $uri
 * @param array $data
 * @param array $headers
 * @return Request
 */
function makeJsonPostRequest(
    string $uri,
    array $data,
    array $headers = []
) {
    $method = "POST";
    $uri = Uri::createFromString($uri);
    $headers['Content-type'] = 'application/json';
    $headers = new Headers($headers);
    $serverParams = [];
    $cookies = [];
    $request = new Request($method, $uri, $headers, $cookies, $serverParams, new Body(makeBody()));
    return $request->withBody(json_encode($data));
}

function makeGetRequest(
    string $uri,
    array $headers = []
) {
    $method = "GET";
    $uri = Uri::createFromString($uri);
    $headers = new Headers($headers);
    $serverParams = [];
    $cookies = [];
    $request = new Request($method, $uri, $headers, $cookies, $serverParams, new Body(makeBody()));
    return $request;
}

function makeResponse()
{
    return new Response();
}

function makeUri(string $url, array $query = [])
{
    return $url . empty($query) ? '' : "&" . http_build_query($query);
}

/**
 * @param string $content
 * @return bool|resource
 */
function makeBody(string $content = "")
{
    $stream = fopen('php://temp', 'w+');
    fwrite($stream, $content);
    return $stream;
}

function getResponse(Response $response)
{
    $header = (array)$response->getHeaders();
    $body = (string)$response->getBody();
    return [
        "header" => $header,
        "body" => $body
    ];
}
