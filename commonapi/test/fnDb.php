<?php

/**
 * @param PDO $pdo
 * @param int $memberId
 * @param int $nbSms
 */
function createMember(\PDO $pdo, int $memberId, int $nbSms)
{
    $stm = $pdo
        ->prepare("insert into account (member_id, nb_sms, comment) values (?,?,?)");
    $stm->execute([
        $memberId,
        $nbSms,
        "Création du membre $memberId avec $nbSms sms."
    ]);
}

/**
 * @param PDO $pdo
 * @param int $memberId
 * @return array
 * @throws Exception
 */
function getMemberID(\PDO $pdo, int $memberId): array
{
    $member = $pdo
        ->query("select * from account where member_id = $memberId order by jour desc")
        ->fetch(\PDO::FETCH_ASSOC);
    if (false === $member) {
        throw new \Exception("Il n'y a pas de MemberId $memberId");
    }
    return $member;
}
function supprimeMember(\PDO $pdo, int $memberId)
{
    $pdo->exec("delete from account where member_id = $memberId");
}
